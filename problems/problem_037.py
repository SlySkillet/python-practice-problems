# Complete the pad_left function which takes three parameters
#   * a number
#   * the number of characters in the result
#   * a padding character
# and turns the number into a string of the desired length
# by adding the padding character to the left of it
#
# Examples:
#   * number: 10
#     length: 4
#     pad:    "*"
#     result: "**10"
#   * number: 10
#     length: 5
#     pad:    "0"
#     result: "00010"
#   * number: 1000
#     length: 3
#     pad:    "0"
#     result: "1000"
#   * number: 19
#     length: 5
#     pad:    " "
#     result: "   19"

def pad_left(number, length, pad):
    #convert number to string
    string = str(number)
    #check length of string
    string_length = len(string)
    #find difference between length of string and desired length of result
    if string_length <= length:
        difference = length - string_length
    else:
        return string
    # add pad according to difference - string method to add character at the beginning of a string?
    result = (pad * difference) + string
    #return result
    return result

print(pad_left(1000, 3, "0"))
