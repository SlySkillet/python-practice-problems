# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
   # check number of values (list length) and store it in a variable
    num_values = len(values)
     # check if list has values
    if num_values == 0: return None
    # sum values and store in a variable
    sum_values = 0
    for value in values:
        sum_values += value

    # divide sum by number of values store in average variable
    average = sum_values / num_values

    # return average
    return average

print(calculate_average([]))
