# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    # check length of list, if empty return None
    if len(values) == 0: return None
    # declare max variable
    max_value = values[0]
    # iterate over list checking each item against the max variable
    for value in values:
        if value >= max_value:
    # update max if item is greater or equal
            max_value = value
    return max_value

print(max_in_list([-4,-6,-3]))
