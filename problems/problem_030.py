# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    #if length of list is 1 or less return none
    length = len(values)
    if length <= 1: return None
    #order list to be largest to smallest
    sorted_list = list(reversed(sorted(values)))
    #return second item of the sorted list
    return sorted_list[1]



print(find_second_largest([18,18,4,5,10,8,9]))
