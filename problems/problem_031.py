# Complete the sum_of_squares function which accepts
# a list of numerical values and returns the sum of
# each item squared
#
# If the list of values is empty, the function should
# return None
#
# Examples:
#   * [] returns None
#   * [1, 2, 3] returns 1*1+2*2+3*3=14
#   * [-1, 0, 1] returns (-1)*(-1)+0*0+1*1=2
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def sum_of_squares(values):
    #check length of list return None if its empty
    if len(values) == 0: return None
    #iterate over list squaring each value and appending to new list
    new_list = []
    for value in values:
        new_list.append(value ** 2)
    print(new_list)
    #return sum of new list
    return sum(new_list)

print(sum_of_squares([-1, 0, 1]))
