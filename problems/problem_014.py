# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    # create a list to contain necessary ingedients
    check_list = 0
    pasta_ingredients = ["flour", "eggs", "oil"]
    for ingredient in ingredients:
        for needed_ingredient in pasta_ingredients:
            if ingredient == needed_ingredient:
                check_list += 1
    return check_list == 3
    # for each ingredient iterate over necessary ingredients to see if it's in necessary ingredients
    #return result
print(can_make_pasta(["flour", "eggs", "oil"]))

#this function depends on no repeated ingredients in the list and all lowercase entry
