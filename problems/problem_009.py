# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    #reverse word and store it in a test variable
    word = word.lower()
    reverse = reversed(word)
    test_word = ""
    for letter in reverse:
        test_word += letter
    #check equality with word
    return test_word == word
    #return True if equal
    #else return false
print(is_palindrome("RacecAr"))
